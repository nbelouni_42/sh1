# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: nbelouni <nbelouni@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2014/11/30 11:19:25 by nbelouni          #+#    #+#              #
#    Updated: 2015/03/07 18:58:46 by nbelouni         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME= ft_minishell1
CC= gcc
CFLAGS= -Wall -Werror -Wextra
HFLAGS= -I includes
LFLAGS= -I libft/includes/
SRC= src/cd.c src/check.c src/command.c src/env.c src/convert_env.c\
	 src/env_modifs.c src/exec_bin.c src/get_next_line.c src/lst.c src/main.c\
	 src/new_path2.c src/path.c src/print_errors.c src/setenv.c src/unsetenv.c\
	 src/ft_get_arg.c
OBJ= cd.o check.o command.o env.o convert_env.o env_modifs.o exec_bin.o\
	 get_next_line.o lst.o main.o new_path2.o path.o print_errors.o setenv.o\
	 unsetenv.o ft_get_arg.o

all: $(NAME)

$(NAME): $(OBJ) 
	$(CC) -o $(NAME) $(OBJ) -L libft -lft

$(OBJ): $(SRC)
	make -C libft
	$(CC) $(CFLAGS) $(HFLAGS) $(LFLAGS) -c $(SRC)

clean:
	rm -f $(OBJ)
	make -C libft clean

fclean: clean
	rm -f $(NAME)
	make -C libft fclean

re: clean all
