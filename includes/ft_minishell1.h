/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_minishell1.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nbelouni <nbelouni@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/08 10:18:27 by nbelouni          #+#    #+#             */
/*   Updated: 2015/03/15 18:16:41 by nbelouni         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_MINISHELL1_H
# define FT_MINISHELL1_H

# include <libft.h>
# include <unistd.h>
# include <sys/types.h>
# include <sys/stat.h>
# include <sys/wait.h>
# include <dirent.h>
# define BUFF_SIZE 1

char	*new_env2(char *s);
char	*check_env_name(t_list *env, char *s);
char	*complete_path(t_list *env, char *path_elem);
int		cmp(t_list *env, char *path_name);
int		where_am_i(t_list *env);
t_list	*convert_env_lst(char **path);
t_list	*cpy_lst(t_list *list);
t_list	*new_elem(char *s, size_t size);
t_list	*move_lst(t_list *env, t_list *tmp, char *var_name);
void	del_lst(t_list **list);
void	del_elem(t_list **elem);
char	**convert_env_tab(t_list *path);
char	**check_arg_bin(t_list *path, char **arg);
void	aff_env(t_list *env);
t_list	*last_command(t_list *env, char *new_cmd);
void	shell_title(t_list *env);
void	shlvl_plus(t_list *env);
char	*cat_arg(char **cmd_elems);
int		get_next_line(int const fd, char **line);
int		exec_bin(t_list *path, char *s);
t_list	*ft_setenv(t_list *env, char *s);
t_list	*ft_unsetenv(t_list *env, char *s);
int		ft_cd(t_list *path, char *s);
void	ft_env(t_list *env, char *path);
int		ft_check_arg(char **argv);
int		first_check(char *arg);
char	*find_var(t_list *env, char *s);
void	minishell(char **path);
t_list	*command(t_list *env, char *base);
int		cd_error(char *file_name, char *path);
void	bin_error(char *bin, int err_n);
void	env_error(char *arg);
void	print_error_option(char c);
int		accessible(char *path);

#endif
