/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   convert_env.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nbelouni <nbelouni@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/23 22:36:30 by nbelouni          #+#    #+#             */
/*   Updated: 2015/05/06 15:00:31 by nbelouni         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_minishell1.h"

static t_list	*env_i(void)
{
	t_list		*list;
	char		buff[1024];
	char		*tmp;
	size_t		size;

	size = 1024;
	getcwd(buff, size);
	list = ft_lstnew("SHELL=./ft_minishell1", 21);
	tmp = ft_strjoin("PWD=", buff);
	list->next = ft_lstnew(tmp, size + 4);
	ft_strdel(&tmp);
	return (list);
}

t_list			*convert_env_lst(char **env)
{
	t_list		*list;
	int			i;

	if (!env || !env[0] ||
	(!ft_strcmp(strbchr(env[0], '='), "_") && !env[1]))
		list = env_i();
	else
	{
		i = 1;
		list = ft_lstnew(env[0], ft_strlen(env[0]));
		while (env[i])
		{
			ft_lstadd_end(list, new_elem(env[i], ft_strlen(env[i])));
			i++;
		}
		shell_title(list);
	}
	return (list);
}

char			**convert_env_tab(t_list *env)
{
	t_list		*tmp;
	char		**new;
	int			len;
	int			i;

	if (!(tmp = env))
		return (NULL);
	len = 0;
	i = 0;
	while (tmp)
	{
		tmp = tmp->next;
		len++;
	}
	if (!(new = (char **)malloc(sizeof(char *) * (len + 1))))
		return (NULL);
	tmp = env;
	while (i < len)
	{
		new[i] = ft_strdup(tmp->content);
		i++;
		tmp = tmp->next;
	}
	new[i] = 0;
	return (new);
}
