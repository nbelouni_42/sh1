/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   new_path2.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nbelouni <nbelouni@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/20 10:05:37 by nbelouni          #+#    #+#             */
/*   Updated: 2015/03/15 19:00:54 by nbelouni         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_minishell1.h"

static char	*add_elem(char **cmd_elems, char *tmp_s, int i)
{
	char	*cmd;
	char	*tmp;

	cmd = ft_strjoin(tmp_s, cmd_elems[i]);
	if (cmd_elems[i + 1])
	{
		tmp = cmd;
		cmd = ft_strjoin(tmp, " ");
	}
	return (cmd);
}

char		*cat_arg(char **cmd_elems)
{
	char	*cmd;
	char	*tmp_s;
	int		i;

	i = 0;
	cmd = NULL;
	while (cmd_elems[i])
	{
		if (!cmd)
		{
			tmp_s = ft_strdup(cmd_elems[i]);
			if (cmd_elems[i + 1])
				cmd = ft_strjoin(tmp_s, " ");
			else
				return (tmp_s);
		}
		else
		{
			tmp_s = cmd;
			cmd = add_elem(cmd_elems, tmp_s, i);
		}
		i++;
	}
	return (cmd);
}

char		*new_env2(char *s)
{
	char	**cmd_elems;
	char	*cmd;

	if (!(cmd_elems = ft_blanksplit(s)))
		return (NULL);
	cmd = cat_arg(cmd_elems);
	return (cmd);
}
