/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cd.c                                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nbelouni <nbelouni@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/10 02:46:09 by nbelouni          #+#    #+#             */
/*   Updated: 2015/03/10 22:45:50 by nbelouni         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_minishell1.h"

static char	*modif_var2(t_list *path, char *var_modif, char *new_content)
{
	t_list	*tmp;
	char	*var_name;
	char	*old_content;
	char	*tmp2;

	tmp = path;
	tmp2 = ft_strjoin(var_modif, "=");
	while (tmp)
	{
		var_name = strbchr(tmp->content, '=');
		if (!ft_strcmp(var_modif, var_name))
		{
			old_content = (tmp->content);
			if (new_content)
				tmp->content = ft_strjoin(tmp2, new_content);
			else
				tmp->content = ft_strdup(tmp2);
			ft_strdel(&tmp2);
			return (old_content);
		}
		tmp = tmp->next;
	}
	return (NULL);
}

static char	*modif_var(t_list *path, char *var_modif, char *new_content)
{
	char	*tmp2;
	char	*tmp1;

	tmp1 = NULL;
	if ((tmp2 = modif_var2(path, var_modif, new_content)))
		return (tmp2);
	tmp2 = ft_strjoin(var_modif, "=");
	if (new_content)
		tmp1 = ft_strjoin(tmp2, new_content);
	ft_lstadd_end(path, new_elem((tmp1) ? tmp1 : tmp2, ft_strlen(tmp1)));
	ft_strdel(&tmp2);
	ft_strdel(&tmp1);
	return (NULL);
}

static int	modif_env(t_list *path, char *current_dir)
{
	char	*tmp;

	tmp = find_var(path, "PWD");
	if ((!current_dir || !*current_dir) && tmp)
	{
		modif_var(path, "PWD", tmp);
		modif_var(path, "OLDPWD", find_var(path, "PWD"));
	}
	else if ((!tmp || !*tmp) && current_dir)
	{
		modif_var(path, "PWD", current_dir);
		modif_var(path, "OLDPWD", find_var(path, "PWD"));
	}
	else
	{
		modif_var(path, "PWD", current_dir);
		modif_var(path, "OLDPWD", tmp);
	}
	return (0);
}

static int	cd2(t_list *path, char *s, char *tmp)
{
	char	*new_path;

	new_path = NULL;
	if (!tmp || !ft_strcmp(tmp, "--") || !ft_strcmp(tmp, "-"))
	{
		if (!tmp || (tmp && !ft_strcmp(tmp, "--")))
			new_path = find_var(path, "HOME");
		if (tmp && !ft_strcmp(tmp, "-"))
			new_path = find_var(path, "OLDPWD");
		if (!new_path)
			return (cd_error(s, new_path));
		chdir(new_path);
		modif_env(path, new_path);
		return (0);
	}
	return (1);
}

int			ft_cd(t_list *path, char *s)
{
	char	*new_path;
	char	*tmp;

	tmp = NULL;
	new_path = NULL;
	path = ft_setenv(path, ft_strjoin("cmd PWD=", getcwd(NULL, 0)));
	if ((strchr(s, ' ')))
		tmp = ft_strtrim(ft_strchr(s, ' '));
	if (cd2(path, s, tmp) < 1)
		return (0);
	else
	{
		tmp = complete_path(path, tmp);
		if (!(new_path = ft_strdup(ft_strchr(s, ' ') + 1)))
			return (-1);
		if (chdir(tmp) == -1)
			return (cd_error(s, new_path));
		new_path = getcwd(NULL, 0);
		modif_env(path, new_path);
	}
	return (0);
}
