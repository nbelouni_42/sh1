/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   env_modifs.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nbelouni <nbelouni@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/28 01:29:06 by nbelouni          #+#    #+#             */
/*   Updated: 2015/03/15 16:46:12 by nbelouni         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_minishell1.h"

static int		var_exists(t_list *env, char *new_cmd)
{
	t_list		*tmp;
	char		*var_name;
	char		*old_cmd;

	tmp = env;
	while (tmp)
	{
		var_name = strbchr(tmp->content, '=');
		if (!ft_strcmp(var_name, "_"))
		{
			old_cmd = tmp->content;
			if (ft_strrchr(new_cmd, ' '))
				tmp->content = ft_strjoin("_=", ft_strrchr(new_cmd, ' ') + 1);
			else
				tmp->content = ft_strjoin("_=", new_cmd);
			ft_strdel(&old_cmd);
			return (0);
		}
		tmp = tmp->next;
	}
	return (-1);
}

t_list			*last_command(t_list *env, char *new_cmd)
{
	char		*new_lst;

	if (ft_strrchr(new_cmd, ' '))
		new_lst = ft_strjoin("_=", ft_strrchr(new_cmd, ' ') + 1);
	else
		new_lst = ft_strjoin("_=", new_cmd);
	if (!env)
		env = new_elem(new_lst, ft_strlen(new_lst));
	else if (!(var_exists(env, new_cmd)))
		return (env);
	else
	{
		new_lst = ft_strjoin("_=", new_cmd);
		ft_lstadd_end(env, new_elem(new_lst, ft_strlen(new_lst)));
	}
	return (env);
}

void			shell_title(t_list *env)
{
	char		*var_name;
	t_list		*tmp;

	tmp = env;
	while (tmp)
	{
		var_name = strbchr(tmp->content, '=');
		if (!strcmp(var_name, "SHELL"))
		{
			ft_strdel((char **)&(tmp->content));
			tmp->content = ft_strdup("SHELL=./ft_minishell1");
			return ;
		}
		ft_strdel(&var_name);
		tmp = tmp->next;
	}
}

void			shlvl_plus(t_list *env)
{
	char		*var_name;
	t_list		*tmp;
	int			shlvl;

	tmp = env;
	while (tmp)
	{
		var_name = strbchr(tmp->content, '=');
		if (!strcmp(var_name, "SHLVL"))
		{
			shlvl = ft_atoi(ft_strchr(tmp->content, '=') + 1);
			ft_strdel((char **)&(tmp->content));
			tmp->content = ft_strjoin("SHLVL=", ft_itoa(shlvl + 1));
			return ;
		}
		tmp = tmp->next;
	}
}
