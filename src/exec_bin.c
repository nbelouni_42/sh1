/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   exec_bin.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nbelouni <nbelouni@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/09 06:28:15 by nbelouni          #+#    #+#             */
/*   Updated: 2015/05/06 15:13:36 by nbelouni         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_minishell1.h"

static t_list		*bin_tab(t_list *env)
{
	t_list			*bin_path;
	char			**tmp_path;
	char			*tmp;

	if (!(tmp = find_var(env, "PATH")))
		return (NULL);
	tmp_path = ft_strsplit(tmp, ':');
	bin_path = convert_env_lst(tmp_path);
	return (bin_path);
}

static char			*add_exec(char *path, char *s)
{
	char			*new;
	char			*tmp;

	tmp = ft_strjoin(path, "/");
	new = ft_strjoin(tmp, s);
	return (new);
}

static int			exec(t_list *bin_path, char **arg, char **env_tmp)
{
	char			*tmp;
	pid_t			first;
	int				ret;
	int				b;

	if (!*arg)
		return (0);
	tmp = (bin_path) ? add_exec(bin_path->content, arg[0]) : NULL;
	if (!(b = accessible(tmp)) || !first_check(arg[0]))
	{
		first = fork();
		if (first > 0)
			waitpid(first, &ret, 0);
		else if (first == 0)
		{
			if (execve((b) ? arg[0] : tmp, arg, env_tmp) == -1)
			{
				bin_error((b) ? arg[0] : tmp, b);
				exit(1);
			}
		}
		return (1);
	}
	return (0);
}

int					exec_bin(t_list *path, char *s)
{
	char			**env_tmp;
	t_list			*bin_path;
	t_list			*tmp;
	char			**arg;

	env_tmp = convert_env_tab(path);
	bin_path = bin_tab(path);
	arg = ft_strsplit(s, ' ');
	arg = check_arg_bin(path, arg);
	if ((tmp = bin_path))
		while (tmp && tmp->content)
		{
			if (exec(tmp, arg, env_tmp))
				return (1);
			tmp = tmp->next;
		}
	else if (exec(tmp, arg, env_tmp))
		return (1);
	if (arg[0] && ft_strcmp("exit", arg[0]))
		bin_error(arg[0], first_check(arg[0]));
	return (0);
}
