/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nbelouni <nbelouni@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/24 19:47:59 by nbelouni          #+#    #+#             */
/*   Updated: 2015/03/15 18:55:18 by nbelouni         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_minishell1.h"

static int			is_dir(const char *path)
{
	struct stat		buf;

	lstat(path, &buf);
	if (S_ISDIR(buf.st_mode))
		return (1);
	return (0);
}

int					first_check(char *arg)
{
	struct stat		*dirs;
	int				ret;

	dirs = (struct stat *)malloc(sizeof(struct stat));
	ret = lstat(arg, dirs);
	if (ft_strchr(arg, '/') && ret == 0)
	{
		free(dirs);
		return (0);
	}
	free(dirs);
	return (2);
}

int					accessible(char *path)
{
	if (is_dir(path))
		return (1);
	else
	{
		if (!access(path, F_OK))
		{
			if (!access(path, X_OK))
				return (0);
			else
				return (1);
		}
		else
			return (2);
	}
	return (3);
}

char				**check_arg_bin(t_list *path, char **arg)
{
	char			*tmp;
	int				i;

	i = 0;
	while (arg[i])
	{
		tmp = arg[i];
		if (ft_strchr(arg[i], '$') || arg[i][0] == '~')
		{
			arg[i] = complete_path(path, arg[i]);
			ft_strdel(&tmp);
		}
		i++;
	}
	return (arg);
}

int					where_am_i(t_list *env)
{
	char			*here;
	char			*pwd;

	here = NULL;
	if (!(here = getcwd(NULL, 1024)))
		return (-1);
	if (!(pwd = find_var(env, "PWD")))
		return (-1);
	if (ft_strcmp(here, pwd))
		if (!(chdir(pwd)))
			return (-1);
	return (0);
}
