/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   command.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nbelouni <nbelouni@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/08 11:07:04 by nbelouni          #+#    #+#             */
/*   Updated: 2015/05/06 15:13:38 by nbelouni         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_minishell1.h"

static char			*redo_last_cmd(t_list *env, char **cmd, char *new_path)
{
	char			*tmp;
	char			*old_path;

	ft_strdel(cmd);
	*cmd = find_var(env, "_");
	tmp = ft_strdup(*cmd);
	if (!strchr(new_path, ' '))
		new_path = tmp;
	else
	{
		old_path = new_path;
		new_path = ft_strjoin(tmp, ft_strchr(old_path, ' '));
		ft_strdel(&old_path);
	}
	return (new_path);
}

t_list				*command(t_list *env, char *base)
{
	char			*new_path;
	char			*cmd;

	cmd = strchr_blank(base);
	new_path = new_env2(base);
	if (!ft_strcmp(cmd, "$_"))
		new_path = redo_last_cmd(env, &cmd, new_path);
	if (!ft_strcmp(cmd, "unsetenv"))
		env = ft_unsetenv(env, new_path);
	env = last_command(env, new_path);
	if (ft_strcmp(cmd, "unsetenv"))
	{
		if (!ft_strcmp(cmd, "env"))
			ft_env(env, new_path);
		else if (!ft_strcmp(cmd, "setenv"))
			env = ft_setenv(env, new_path);
		else if (!ft_strcmp(cmd, "cd"))
			ft_cd(env, new_path);
		else if (*new_path && ft_strcmp(new_path, "exit"))
		{
			if (exec_bin(env, new_path) < 1)
				bin_error(new_path, 0);
		}
	}
	return (env);
}

static int			is_digit(char *base)
{
	int				blank;
	int				i;

	i = 0;
	blank = 0;
	while (base[i])
	{
		blank += ft_isblank(base[i]);
		i++;
	}
	return (ft_strlen(base) - blank);
}

static t_list		*exec_args(t_list *env, char **base, int *b)
{
	char			*tmp;

	if (!(is_digit(*base)))
		return (env);
	tmp = *base;
	*base = ft_strtrim(tmp);
	if (*base && !ft_strcmp(*base, "exit"))
		*b = 1;
	env = command(env, *base);
	ft_strdel(&tmp);
	return (env);
}

void				minishell(char **path)
{
	t_list			*env;
	char			*s;
	char			**base;
	int				b;
	int				i;

	s = NULL;
	b = 0;
	base = NULL;
	env = convert_env_lst(path);
	shlvl_plus(env);
	while (!base || (*base && ft_strcmp(*base, "exit")) || !b)
	{
		(s) ? ft_strdel(&s) : (void)s;
		base = (base) ? ft_free_ctab(base) : NULL;
		ft_putstr_col(strrchr(getcwd(NULL, 0), '/') + 1, CYAN, NULL, 1);
		ft_putstr_col(" > ", GREEN, GRAS, 1);
		if ((i = -1), get_next_line(0, &s) == 0)
			return (ft_putchar('\n'));
		if (s && *s != 0 && (base = ft_strsplit(s, ';')))
		{
			while (base[++i] != 0 && !b)
				env = exec_args(env, &(base[i]), &b);
		}
	}
}
