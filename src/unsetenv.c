/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   unsetenv.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nbelouni <nbelouni@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/22 13:17:59 by nbelouni          #+#    #+#             */
/*   Updated: 2015/05/06 15:01:33 by nbelouni         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_minishell1.h"

static t_list	*supp_elem2(t_list *env, char *elem_name)
{
	t_list		*tmp1;
	t_list		*tmp2;
	char		*env_name;

	tmp1 = env;
	tmp2 = tmp1->next;
	while (tmp2)
	{
		tmp2 = tmp1->next;
		env_name = strbchr((tmp2) ? tmp2->content : tmp1->content, '=');
		if (!ft_strcmp(elem_name, env_name))
		{
			tmp1->next = tmp2->next;
			del_elem(&tmp2);
			return (env);
		}
		tmp1 = tmp1->next;
	}
	return (env);
}

static t_list	*supp_elem(t_list *env, char *elem)
{
	t_list		*tmp1;
	char		*elem_name;
	char		*env_name;

	tmp1 = env;
	elem_name = strbchr(elem, '=');
	env_name = strbchr(tmp1->content, '=');
	if (!ft_strcmp(elem_name, env_name))
	{
		env = env->next;
		del_elem(&tmp1);
		return (env);
	}
	env = supp_elem2(env, elem_name);
	return (env);
}

t_list			*ft_unsetenv(t_list *env, char *s)
{
	t_list		*tmp;
	char		**elem;
	char		*var_name;
	int			i;

	if (!env)
		return (NULL);
	if (!(elem = ft_strsplit(s, ' ')))
		return (env);
	i = 1;
	while (elem[i])
	{
		if (!(var_name = strbchr(elem[i], '=')))
			return (env);
		tmp = env;
		tmp = move_lst(env, tmp, var_name);
		if (tmp)
		{
			if (!(env = supp_elem(env, elem[i])))
				return (NULL);
		}
		i++;
	}
	return (env);
}
