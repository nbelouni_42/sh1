/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   path.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nbelouni <nbelouni@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/20 10:47:13 by nbelouni          #+#    #+#             */
/*   Updated: 2015/03/15 19:01:23 by nbelouni         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_minishell1.h"

char		*find_var(t_list *env, char *s)
{
	t_list	*tmp;
	char	*var_name;
	char	*path;
	int		i;

	i = 0;
	tmp = env;
	while (tmp)
	{
		var_name = strbchr(tmp->content, '=');
		if (!ft_strcmp(var_name, s))
		{
			path = ft_strdup(ft_strchr(tmp->content, '=') + 1);
			return (path);
		}
		tmp = tmp->next;
		i++;
	}
	return (NULL);
}

static int	check_path2(char *s)
{
	DIR		*dirp;

	if ((dirp = opendir(s)))
	{
		closedir(dirp);
		return (0);
	}
	return (-1);
}

static char	*replace_env(t_list *env, char *path_elem)
{
	char	*new_path;

	new_path = find_var(env, path_elem + 1);
	return (new_path);
}

char		*join_elems(t_list *env, char **elems, char *path)
{
	int		i;

	i = 0;
	if (!ft_strcmp(elems[0], "~"))
		i = 1;
	while (elems[i])
	{
		if (elems[i][0] == '$')
			path = replace_env(env, elems[i]);
		else
		{
			if (!path)
				path = ft_strdup(elems[i]);
			else
				path = ft_strjoin(path, elems[i]);
		}
		if (elems[i + 1])
		{
			path = ft_strjoin(path, "/");
		}
		i++;
	}
	return (path);
}

char		*complete_path(t_list *env, char *s)
{
	char	**elems;
	char	*path;

	path = NULL;
	if ((check_path2(s)) == 0)
		return (s);
	elems = ft_strsplit(s, '/');
	if (!ft_strcmp(elems[0], "~") || !ft_strcmp(elems[0], "."))
	{
		path = find_var(env, "HOME");
		if (elems[1])
		{
			path = ft_strjoin(path, "/");
		}
	}
	path = join_elems(env, elems, path);
	return (path);
}
