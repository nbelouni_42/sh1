/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_errors.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nbelouni <nbelouni@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/13 00:47:59 by nbelouni          #+#    #+#             */
/*   Updated: 2015/03/11 02:26:21 by nbelouni         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_minishell1.h"

int				cd_error(char *file_name, char *path)
{
	struct stat	*dirs;

	if (file_name && path)
	{
		if (!(dirs = (struct stat *)malloc(sizeof(struct stat))))
			return (0);
		if (!lstat(path, dirs))
		{
			if (!(dirs->st_mode & S_IXUSR))
				ft_putstr_fd("cd: permission denied: ", 2);
		}
		else
			ft_putstr_fd("cd: no such file or directory: ", 2);
		ft_putendl_fd(ft_strrchr(file_name, ' ') + 1, 2);
		free(dirs);
	}
	else
		ft_putendl_fd("cd : var not set", 2);
	return (-1);
}

void			bin_error(char *bin, int err_n)
{
	char		*tmp;

	if (err_n == 0)
		return ;
	tmp = strbchr(bin, ' ');
	if (err_n == 1)
		ft_putstr_fd("ft_sh1: permission denied: ", 2);
	else if (err_n == 2)
		ft_putstr_fd("ft_sh1: : command not found: ", 2);
	else if (err_n == 3)
		ft_putstr_fd("ft_sh1: : no such file or directory: ", 2);
	ft_putendl_fd(tmp, 2);
	ft_strdel(&tmp);
}

void			env_error(char *arg)
{
	ft_putstr_fd("env: ", 2);
	ft_putstr_fd(arg, 2);
	ft_putendl_fd("command not found: ", 2);
}

void			print_error_option(char c)
{
	ft_putstr_fd("env: illegal option -- ", 2);
	ft_putchar_fd(c, 2);
	ft_putstr_fd("\nusage: env [-i] [name=value ...] ", 2);
	ft_putstr_fd("[utility [arguement ...]]\n", 2);
}
