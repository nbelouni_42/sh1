/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lst.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nbelouni <nbelouni@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/24 16:42:36 by nbelouni          #+#    #+#             */
/*   Updated: 2015/03/15 19:00:13 by nbelouni         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_minishell1.h"

void			del_lst(t_list **list)
{
	t_list		*tmp;

	while (*list)
	{
		tmp = *list;
		*list = (*list)->next;
		del_elem(&tmp);
	}
}

void			del_elem(t_list **elem)
{
	free((*elem)->content);
	free(*elem);
}

t_list			*new_elem(char *s, size_t size)
{
	t_list		*new;

	if (!(new = (t_list *)malloc(sizeof(t_list))))
		return (NULL);
	new->content = ft_strdup(s);
	new->content_size = size;
	new->next = NULL;
	return (new);
}

t_list			*cpy_lst(t_list *list)
{
	t_list		*tmp;
	t_list		*new;

	new = new_elem(list->content, ft_strlen(list->content));
	if (list->next)
	{
		tmp = list->next;
		while (tmp)
		{
			ft_lstadd_end(new, new_elem(tmp->content, ft_strlen(tmp->content)));
			tmp = tmp->next;
		}
	}
	return (new);
}
