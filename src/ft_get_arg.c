/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_get_arg.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nbelouni <nbelouni@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/22 23:16:17 by nbelouni          #+#    #+#             */
/*   Updated: 2015/03/07 19:42:56 by nbelouni         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_minishell1.h"

static int			count_arg(int arg, int c)
{
	if (c == 'i')
		arg += 1;
	else
		return (0);
	return (1);
}

static int			stock_arg(char *argv, int arg)
{
	int				i;
	int				b;

	i = 1;
	while (argv[i])
	{
		if (argv[1] == '-')
		{
			if (argv[2] != '\0')
				print_error_option('-');
			return (-1);
		}
		b = count_arg(arg, argv[i]);
		if (!b)
		{
			print_error_option(argv[i]);
			return (-1);
		}
		i++;
	}
	return (i);
}

int					ft_check_arg(char **argv)
{
	int				i;
	int				arg;
	int				check;
	int				ref;

	i = 0;
	arg = 0;
	check = 0;
	while (argv[i])
	{
		ref = check;
		if (argv[i][0] == '-')
			check += stock_arg(argv[i], arg);
		if (check < ref)
			return (-1);
		else if (check == ref)
			break ;
		i++;
	}
	return (check + i);
}
