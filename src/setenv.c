/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   setenv.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nbelouni <nbelouni@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/21 17:25:09 by nbelouni          #+#    #+#             */
/*   Updated: 2015/03/11 07:31:57 by nbelouni         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_minishell1.h"

char		*modif_env(t_list *env, char *elem)
{
	char	*tmp;

	if (!(ft_strchr(elem, '=')))
		return (NULL);
	tmp = env->content;
	env->content = ft_strdup(elem);
	ft_strdel(&tmp);
	return (env->content);
}

t_list		*move_lst(t_list *env, t_list *tmp, char *var_name)
{
	int		val;

	val = cmp(env, var_name);
	while (val)
	{
		if (tmp)
			tmp = tmp->next;
		val--;
	}
	return (tmp);
}

static char	*get_var_name(char *s, int c)
{
	int		i;
	char	*new;

	i = 0;
	while (s[i])
	{
		if (s[i] == c)
			break ;
		i++;
	}
	if (!s[i])
		return (NULL);
	new = (char *)malloc(sizeof(char) * i);
	ft_strncpy(new, s, i);
	new[i] = '\0';
	return (new);
}

static char	**new_tab(t_list *env, char *s)
{
	char	**elem;

	if (!(elem = ft_strsplit(s, ' ')))
		return (NULL);
	if (!elem[1])
	{
		aff_env(env);
		return (NULL);
	}
	return (elem);
}

t_list		*ft_setenv(t_list *env, char *s)
{
	t_list	*tmp;
	char	**elem;
	char	*var_name;
	int		i;

	if ((elem = new_tab(env, s)))
	{
		i = 0;
		while (elem[++i] && (var_name = get_var_name(elem[i], '=')))
		{
			if (!(tmp = env))
				env = new_elem(elem[i], ft_strlen(elem[i]));
			else
			{
				tmp = move_lst(env, tmp, var_name);
				if (tmp && tmp->content)
					tmp->content = modif_env(tmp, elem[i]);
				else if (ft_strchr(elem[i], '=') && ft_strcmp(elem[i], "="))
					ft_lstadd_end(env, new_elem(elem[i], ft_strlen(elem[i])));
			}
			ft_strdel(&var_name);
		}
	}
	return (env);
}
