/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   env.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nbelouni <nbelouni@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/09 06:44:50 by nbelouni          #+#    #+#             */
/*   Updated: 2015/03/15 19:04:13 by nbelouni         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_minishell1.h"

void			aff_env(t_list *env)
{
	t_list		*tmp;

	if (!env)
		return ;
	tmp = env;
	while (tmp)
	{
		ft_putendl(tmp->content);
		tmp = tmp->next;
	}
}

static t_list	*tmp_replace_var(t_list *env, char **arg, int *b)
{
	char		*new_arg;
	int			line_cmd;
	int			i;

	i = 0;
	new_arg = cat_arg(arg);
	if ((line_cmd = ft_check_arg(arg)) > 0)
		env = NULL;
	if ((*b = line_cmd) < 0 || line_cmd >= (int)ft_strlen(new_arg))
		return (env);
	if (strchr(new_arg, '='))
	{
		while (ft_strchr(arg[i], '-') && !ft_strchr(arg[i], '='))
			i++;
		while (ft_strchr(arg[i], '='))
			env = ft_setenv(env, ft_strjoin("cmd ", arg[i++]));
		new_arg = (!arg[i]) ? ft_strdup("env") : cat_arg(arg + i);
		line_cmd = 0;
	}
	*b = 1;
	if (new_arg + line_cmd)
		command(env, new_arg + ((line_cmd >= 0) ? line_cmd : 0));
	return (env);
}

int				cmp(t_list *env, char *path_name)
{
	t_list		*tmp;
	char		*var_name;
	int			i;

	i = 0;
	var_name = NULL;
	tmp = env;
	while (tmp)
	{
		var_name = strbchr(tmp->content, '=');
		if (!ft_strcmp(var_name, path_name))
			break ;
		tmp = tmp->next;
		i++;
	}
	return (i);
}

static t_list	*modif_tmp_env(t_list *env_tmp, char *path)
{
	char		**cmd_elem;
	char		*tmp;
	char		*path_name;
	int			i;
	int			b;

	i = 0;
	b = 0;
	cmd_elem = ft_strsplit(ft_strchr(path, ' ') + 1, ' ');
	while (cmd_elem[i] && b == 0)
	{
		path_name = strbchr(cmd_elem[i], '=');
		tmp = path_name;
		path_name = ft_strtrim(path_name);
		env_tmp = tmp_replace_var(env_tmp, cmd_elem + i, &b);
		i++;
		ft_strdel(&tmp);
	}
	if (b == 0)
		aff_env(env_tmp);
	ft_free_ctab(cmd_elem);
	return (env_tmp);
}

void			ft_env(t_list *env, char *path)
{
	t_list		*env_tmp;

	if (!env)
		return ;
	env_tmp = cpy_lst(env);
	if (!ft_strchr(path, ' '))
		aff_env(env_tmp);
	else
		env_tmp = modif_tmp_env(env_tmp, path);
	if (env && !env->content)
		env_error(ft_strchr(path, ' ') + 1);
	where_am_i(env);
	del_lst(&env_tmp);
}
