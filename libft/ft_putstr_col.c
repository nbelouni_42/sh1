/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putstr_col.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nbelouni <nbelouni@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/11 06:56:30 by nbelouni          #+#    #+#             */
/*   Updated: 2015/03/11 07:10:13 by nbelouni         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void		ft_putstr_col(char *s, char *color, char *effet, int fd)
{
	ft_putstr_fd(color, fd);
	if (effet)
		ft_putstr_fd(effet, fd);
	ft_putstr_fd(s, fd);
	ft_putstr_fd(END, fd);
}
