/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lst_add_end.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nbelouni <nbelouni@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/23 22:41:09 by nbelouni          #+#    #+#             */
/*   Updated: 2015/01/24 00:48:06 by nbelouni         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void		ft_lstadd_end(t_list *list, t_list *alst)
{
	t_list	*tmp;

	if (!list)
		return ;
	tmp = list;
	while (tmp->next)
		tmp = tmp->next;
	tmp->next = alst;
}
