/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strfjoin.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nbelouni <nbelouni@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/09 00:03:55 by nbelouni          #+#    #+#             */
/*   Updated: 2015/01/09 00:09:45 by nbelouni         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static char	*ft_cpy(char *s, char *new)
{
	int		j;

	j = 0;
	while (s[j])
	{
		new[j] = s[j];
		j++;
	}
	new[j] = '\0';
	return (new);
}

char		*ft_strfjoin(char *s1, char *s2)
{
	char	*new;

	if (!s1 || !s2)
		return (NULL);
	new = (char *)malloc(sizeof(char) * (ft_strlen(s1) + ft_strlen(s2) + 1));
	if (!new)
		return (NULL);
	new = ft_cpy(s1, new);
	new = ft_cpy(s2, new + ft_strlen(s1) - 1);
	ft_strdel(&s1);
	ft_strdel(&s2);
	return (new);
}
