/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_blanksplit.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nbelouni <nbelouni@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/20 10:21:58 by nbelouni          #+#    #+#             */
/*   Updated: 2015/05/06 15:00:06 by nbelouni         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int	ft_parcer(char const *s, int i)
{
	while (s[i] && (s[i] != ' ' && s[i] != '\t' && s[i] != '\n'))
		i++;
	return (i);
}

static int	tablen(char const *s)
{
	int		i;
	int		len;

	i = 0;
	len = 0;
	while (s[i])
	{
		if ((s[i] != ' ' && s[i] != '\t' && s[i] != '\n') &&
		(!s[i + 1] ||
		(s[i + 1] == ' ' || s[i + 1] == '\t' || s[i + 1] == '\n')))
			len++;
		i++;
	}
	return (len);
}

char		**ft_blanksplit(char const *s)
{
	int		start;
	int		i;
	int		len;
	char	**new;

	len = 0;
	i = 0;
	start = 0;
	if (!s)
		return (NULL);
	new = (char **)malloc(sizeof(char *) * (tablen(s) + 1));
	if (!new)
		return (NULL);
	while (i < tablen(s))
	{
		len += start;
		while (s[len] == ' ' || s[len] == '\t' || s[len] == '\n')
			len++;
		start = ft_parcer(s, len) - len;
		new[i] = ft_strsub((const char *)s, len, start);
		i++;
	}
	new[i] = 0;
	return (new);
}
